@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                @include('_includes.navi')
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">Home</div>

                    <div class="card-body">

                        {{--
                        Fortschritt Fragebogen
                        <div class="progress" style="margin-bottom: 15px;">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                                 aria-valuemin="0" aria-valuemax="100">25%
                            </div>
                        </div>

                        Fortschritt Bewerberprozess
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 15%;" aria-valuenow="15"
                                 aria-valuemin="0" aria-valuemax="100">25%
                            </div>
                        </div>
                        --}}
                        <div>
                            <h4 style="margin-top: 30px;">Account</h4>
                            <b>Vorname:</b> {{$applicant['account']['FirstName']}}<br/>
                            <b> Nachname:</b> {{$applicant['account']['LastName']}}<br/>
                            <br/>
                            <b>Strasse:</b> {{$applicant['account']['ShippingStreet']}}<br/>
                            <b>PLZ:</b> {{$applicant['account']['ShippingPostalCode']}}<br/>
                            <b>Stadt:</b> {{$applicant['account']['ShippingCity']}}<br/>
                            <b>Land:</b> {{$applicant['account']['ShippingCountry']}}<br/>
                            <br/>
                            <b>Telefon:</b> {{$applicant['account']['Phone']}}<br/>
                            <b>Email:</b> {{$applicant['account']['EmailAddress1__c']}}<br/>

                            <h4 style="margin-top: 30px;">Opportunity</h4>
                            <a target="_blank"
                               href="https://careerpartner--uat.lightning.force.com/lightning/r/Opportunity/{{$applicant['opportunity']['Id']}}/view">
                                {{$applicant['opportunity']['Id']}}<br/>
                            </a>

                            {{$applicant['opportunity']['Name']}}<br/>
                            {{$applicant['opportunity']['Campus__c']}}<br/>

                            <h4 style="margin-top: 30px;">Archives</h4>
                            @foreach($applicant['archives'] as $archive)
                                {{$archive['Name']}}
                                @if($archive['PODLink__c'])
                                    <a target="_blank" href="{{$archive['PODLink__c']}}">
                                        Link PDF
                                    </a>
                                @endif
                                <br/>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('_includes.advisor')
            </div>
        </div>
    </div>
@endsection
