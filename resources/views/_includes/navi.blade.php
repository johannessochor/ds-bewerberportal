<div class="card">
    <div class="card-header">Navigation</div>
    <div class="card-body">
        <ul style="padding: 0;">
            <li style="list-style: none;">
                <a href="/home">
                    Home
                </a>
            </li>
            <li style="list-style: none;">
                <a href="/questionnaire">
                    Fragebogen
                </a>
            </li>
            <li style="list-style: none;">
                <a href="/practical-partner">
                    Praxispartner
                </a>
            </li>
            <li style="list-style: none;">
                Bewerbungstipps
            </li>
            <li style="list-style: none;">
                Termine
            </li>
            <li style="list-style: none;">
                <a href="/documents">
                    Deine Unterlagen
                </a>
            </li>
            <li style="list-style: none;">
                <a href="/user">
                    Dein Profil
                </a>
            </li>

        </ul>
    </div>
</div>
