<div class="card">
    <div class="card-header">Ansprechpartner</div>

    <img src="{{$user->FullPhotoUrl}}">

    <div style="padding: 5px; padding-top: 15px">
        {{$user->Name}}<br/>
        {{$user->Title}}<br/>
        {{$user->CompanyName}}
        <div>
            So erreichst Du mich persönlich:<br/>

            <br/>
            <p style="font-size: 14px">
                {{$user->Email}}<br/>
                {{$user->Phone}}<br/>
                {{$user->MobilePhone}}
                <a target="_blank" href="https://wa.me/{{$user->MobilePhone}}" style="color:#4D933D">
                    (auch über WhatsApp)
                </a>
                <br/>
            </p>

            Erreichbar von Montag - Freitag von 8 - 19 Uhr
        </div>

    </div>
</div>
