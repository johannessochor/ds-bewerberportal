@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                @include('_includes.navi')
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dein Profil</div>

                    @isset(request()->success)
                        <div class="alert alert-success">
                            Daten geändert!
                        </div>
                    @endisset

                    <form action="/user" method="POST" style="padding: 25px;">
                        @csrf

                        <input hidden name="Id" value="{{$applicant['account']['Id']}}">

                        <div class="form-group">
                            <label>Vorname</label>
                            <input class="form-control" value="{{$applicant['account']['FirstName']}}" name="FirstName">
                        </div>

                        <div class="form-group">
                            <label>Nachname</label>
                            <input class="form-control" value="{{$applicant['account']['LastName']}}" name="LastName">
                        </div>

                        <div class="form-group">
                            <label>ShippingStreet</label>
                            <input class="form-control" value="{{$applicant['account']['ShippingStreet']}}"
                                   name="ShippingStreet">
                        </div>

                        <div class="form-group">
                            <label>ShippingPostalCode</label>
                            <input class="form-control" value="{{$applicant['account']['ShippingPostalCode']}}"
                                   name="ShippingPostalCode">
                        </div>

                        <div class="form-group">
                            <label>ShippingCity</label>
                            <input class="form-control" value="{{$applicant['account']['ShippingCity']}}"
                                   name="ShippingCity">
                        </div>

                        <div class="form-group">
                            <label>Phone</label>
                            <input class="form-control" value="{{$applicant['account']['Phone']}}"
                                   name="Phone">
                        </div>

                        <button class="btn btn-primary">
                            Speichern
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
