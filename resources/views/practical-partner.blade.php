@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                @include('_includes.navi')
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Praxispartner</div>

                    <div class="card-body">
                        <h3>Zufällige Praxispartner</h3>
                        <table style="width: 100%;">
                            @foreach($accounts as $account)
                                <tr style="width: 100%;">
                                    <td width="33%">{{substr($account->Name, 0, 30)}}</td>
                                    <td width="33%">
                                        {{$account->ShippingCity}}
                                        {{$account->ShippingPostalCode}}
                                    </td>
                                    <td width="33%">{{round($account->distance, 0)}} km</td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
