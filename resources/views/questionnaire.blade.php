@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                @include('_includes.navi')
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Fragebogen</div>

                    <div class="card-body">
                        <form action="/questionnaire" method="POST">
                            @csrf

                            @foreach($questions as $question)
                                <h4 style="padding-top: 15px;">{{$question->Title__c}}</h4>
                                <h5>{{$question->DescriptionForStudents__c}}</h5>

                                @if($question->EntryType__c == 'Number')
                                    <div class="form-group">
                                        <input name="{{$question->Id}}" value="{{$question->value}}" type="number"
                                               class="form-control">
                                    </div>
                                @endif

                                @if($question->EntryType__c == 'Checkbox')
                                    <select name="{{$question->Id}}" class="form-control">
                                        <option></option>
                                        <option @if($question->value === 'Ja') selected @endif value="Ja">
                                            Ja
                                        </option>
                                        <option @if($question->value === 'Nein') selected @endif value="Nein">
                                            Nein
                                        </option>
                                    </select>
                                @endif

                                @if($question->EntryType__c == 'Multi-Select Picklist')
                                    <select name="{{$question->Id}}" class="form-control">
                                        @foreach($question->picklistValues as $picklistValue)
                                            <option>{{$picklistValue->Value__c}}</option>
                                        @endforeach
                                    </select>
                                @endif

                                @if($question->EntryType__c == 'Picklist')
                                    <select name="{{$question->Id}}" class="form-control">
                                        @foreach($question->picklistValues as $picklistValue)
                                            <option>{{$picklistValue->Value__c}}</option>
                                        @endforeach
                                    </select>
                                @endif

                                @if($question->answer)
                                    <span style="color: red">
                                        {{$question->answer->CriteriaValue__c}}
                                    </span>
                                @endisset
                            @endforeach

                            <div>
                                <button type="submit" class="btn btn-primary">Speichern</button>
                            </div>
                        </form>

                        <form action="/questionnaire" method="POST" style="opacity: 0.2; margin-top: 80px;">
                            @csrf
                            <h4>Motivation</h4>
                            <div class="form-group">
                                <label for="question1">
                                    Warum möchtest Du studieren? Was sind denn Deine Erwartungen an das Studium? Wo
                                    siehst
                                    Du bei Dir Stärken und Schwächen?
                                </label>
                                <textarea id="question1" name="question1" rows="3" class="form-control"></textarea>
                            </div>

                            <h4>Vorkenntnisse</h4>
                            <div class="form-group">
                                <label for="question2">
                                    Hast Du bereits berufliche Erfahrungen insbesondere in der Branche gesammelt z. B.
                                    durch
                                    eine Ausbildung oder ein Praktikum? </label>
                                <textarea id="question2" name="question2" rows="3" class="form-control"></textarea>
                            </div>

                            <h4>Flexibilität</h4>

                            <label>
                                Hast Du einen Führerschein und besitzt Du ein Auto?
                            </label>
                            <div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="question3" id="question3"
                                           value="true">
                                    <label class="form-check-label" for="question3">Ja</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="question3" id="question4"
                                           value="false">
                                    <label class="form-check-label" for="question4">Nein</label>
                                </div>
                            </div>
                            <label style="padding-top: 15px;">
                                In welchem Ort möchtest Du studieren und arbeiten und wie weit wärst Du ggfs. bereit,
                                zum Praxispartner zu fahren
                            </label>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="question5">Ort</label>
                                        <input name="question5" id="question5" class="form-control">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="question6">Umkreis in km</label>
                                        <input name="question6" id="question6" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <h4>HZB</h4>
                            <label>
                                Hast Du bzw. wirst du in Kürze Abitur, Fachabitur (und was es da sonst noch gibt)
                                machen?
                            </label>
                            <div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="question7" id="question7"
                                           value="true">
                                    <label class="form-check-label" for="question7">Ja</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="question7" id="question8"
                                           value="false">
                                    <label class="form-check-label" for="question8">Nein</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Speichern</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
