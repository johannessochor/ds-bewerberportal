@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                @include('_includes.navi')
            </div>
            <div class="col-md-10">
                <div class="card">

                    <div class="alert alert-info">
                        Bisher hast Du {{count($files)}} Dateien hochgeladen.
                    </div>

                    <ul>
                        @foreach($files as $file)
                            <li>
                                <a target="_blank" href="/document/{{ $file[0]['Id']}}">
                                    {{ $file[0]['Title']}}
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="card-header">Deine Unterlagen</div>
                    <div class="card-body">
                        <form action="/documents" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>
                                    Datei auswählen
                                </label>
                                <div class="error-message">
                                    @error('upload') {{$message}} @enderror
                                </div>

                                <input class="form-control-file" name="upload" type="file">

                                <button class="btn btn-primary mg-top-15" type="submit">
                                    Upload
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
