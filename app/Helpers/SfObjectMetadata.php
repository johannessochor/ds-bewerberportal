<?php

namespace App\Helpers;

use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;

class SfObjectMetadata
{
    /**
     * Get all columns of a SF sObject
     *
     * @param string $sObject
     * @return array
     */
    public static function getColumns(string $sObject): array
    {
        $fields = [];
        Forrest::authenticate();
        $return = Forrest::sobjects($sObject . '/describe');

        foreach ($return['fields'] as $field) {
            $fields[] = $field['name'];
        }

        return $fields;
    }
}
