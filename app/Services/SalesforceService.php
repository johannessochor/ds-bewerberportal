<?php

namespace App\Services;

use App\Models\Salesforce\Account;
use App\Models\Salesforce\MatchingCatalogOption;
use App\Models\Salesforce\MatchingCatalogPicklistValue;
use App\Models\Salesforce\Opportunity;
use App\Models\Salesforce\OpportunityArchive;
use App\Models\Salesforce\StudentsSkill;
use App\Models\Salesforce\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;

// TODO remove auth()->user() and replace with parameter
class SalesforceService
{
    // TODO make eloquent models with Relations ...
    public function getSalesforceData($opportunityId = null): array
    {
        if (!$opportunityId) {
            $opportunityId = auth()->user()->opportunity_id;
        }

        return Cache::remember($opportunityId, 900, function () use ($opportunityId) { // 15 Minutes
            $opportunity = Opportunity::select('Id', 'AccountId', 'ObwUserKey__c', 'Name', 'Campus__c',
                'StudyAdvisor__c')
                ->find($opportunityId)->mapPicklistValues();

            $account = Account::select('Id', 'FirstName', 'LastName', 'ShippingStreet', 'ShippingCity',
                'ShippingCountry', 'ShippingPostalCode', 'Phone', 'EmailAddress1__c')
                ->find($opportunity['AccountId'])->mapPicklistValues();

            $archives = OpportunityArchive::select('Id', 'PODLink__c', 'Name')
                ->where('Opportunity__c', $opportunity['Id'])
                ->get()->toArray();

            return [
                'account' => $account,
                'opportunity' => $opportunity,
                'archives' => $archives,
            ];
        });
    }

    // TODO Eloquent Models etc. Custom Salesforce Endpoint?
    public function getOpportunityFiles(): array
    {
        Forrest::authenticate();
        $documentLinks = Forrest::query("SELECT Id, ContentDocumentId, LinkedEntityId From ContentDocumentLink
            WHERE LinkedEntityId = '" . auth()->user()->opportunity_id . "'");

        $files = [];
        foreach ($documentLinks['records'] as $documentLink) {
            $record = Forrest::query("SELECT Id, VersionData, FileExtension, Title, CreatedById, FileType
                    FROM ContentVersion
                    WHERE ContentDocumentId='" . $documentLink['ContentDocumentId'] . "'");
            $files[] = $record['records'];
        }

        return $files;
    }

    // TODO make config etc.
    public function getOpportunityAdvisor($advisor): object
    {
        if ($advisor) {
            $user = User::select('Id', 'Name', 'Title', 'CompanyName', 'Phone',
                'FullPhotoUrl', 'MobilePhone', 'Email')
                ->find($advisor);
        }

        if (!isset($user)) {
            $user = new User();
            $user->Name = 'Maria';
            $user->Title = 'Deine Beraterin rund ums Studium';
            $user->CompanyName = 'IUBH Duales Studium';
            $user->Phone = '+49 (800) 600 161 67';
            $user->FullPhotoUrl = '/img/default.png';
            $user->Email = 'info@iubh-dualesstudium.de';
            $user->MobilePhone = '004915256602498';
        }

        return $user;
    }

    // TODO: make own Service in a composer package - make request validation, test etc.
    public function fileUpload($request): array
    {
        $sfData = $this->getSalesforceData();
        $file = [];

        $file['fileName'] = pathinfo($request->upload->getClientOriginalName(), PATHINFO_FILENAME);
        $file['fileExtension'] = pathinfo($request->upload->getClientOriginalName(), PATHINFO_EXTENSION);
        $file['entityType'] = 'OPPORTUNITY';
        $file['hashKey'] = $sfData['opportunity']['ObwUserKey__c'];
        $file['recordId'] = auth()->user()->opportunity_id;
        $request->upload = file_get_contents($request->upload);
        $file['base64File'] = base64_encode($request->upload);

        $queryParams = [
            'fileName' => $file['fileName'],
            'fileExtension' => $file['fileExtension'],
            'entityType' => $file['entityType'],
            'recordId' => $file['recordId'],
            'hashKey' => $file['hashKey'],
        ];

        $url = '/v1/fileupload?';
        foreach ($queryParams as $key => $param) {
            $url .= '&' . $key . '=' . $param;
        }

        Forrest::authenticate();
        return Forrest::custom($url, [
            'method' => 'POST',
            'headers' => [
                'Content-Type' => '*'
            ],
            'body' => base64_decode($file['base64File'])
        ]);
    }

    // TODO handle with Forrest / Eloquent
    public function getFile($id)
    {
        return Http::withToken('00D0Q0000008fJE!AQIAQDgXI6Vs3xv_ZTVTZXbNahCjXIxHkBuOG3bOPtqLB47RT97GoPwSu_Ruq.adoCqzqj6ZqkVzK6t98DOjlkqoOpUNw900')
            ->get(env('SF_DOMAIN_URL') . '/services/data/v49.0/sobjects/ContentVersion/' . $id . '/VersionData');
    }

    // TODO Salesforce Endpoint!
    public function getQuestionnaire()
    {
        $skillsByCriteria = [];

        $skills = StudentsSkill::select('Id, Criteria__c, CriteriaLabel__c, CriteriaValue__c, Disabled__c, IsPerimeter__c,
                IsRequirement__c, Opportunity__c, Name, Optional__c')
            ->where('Opportunity__c', auth()->user()->opportunity_id)
            ->get();
        foreach ($skills as $skill) {
            $skillsByCriteria[$skill->Criteria__c] = $skill;
        }

        $questions = MatchingCatalogOption::select('Id', 'CriteriaType__c', 'Deactivated__c', 'DescriptionForJobs__c',
            'DescriptionForStudents__c', 'EntryType__c', 'ForStudentsOptional__c', 'Title__c')
            ->where('IsActive__c', true)
            ->get();

        $picklists = ['Picklist', 'Multi-Select Picklist'];
        foreach ($questions as $question) {
            if (isset($skillsByCriteria[$question->Id])) {
                $question->answer = $skillsByCriteria[$question->Id];
                $question->value = $skillsByCriteria[$question->Id]->CriteriaValue__c;
            }
            if (in_array($question->EntryType__c, $picklists)) {
                $question->picklistValues = MatchingCatalogPicklistValue::select('Id, ExternalId__c, MatchingCatalogOption__c,
                    Name, Value__c')
                    ->where('MatchingCatalogOption__c', $question->Id)
                    ->get();
            }
        }

        return $questions;
    }

    // TODO - Salesforce Endpont!
    public function updateQuestionnaire($request)
    {
        $questions = $request->all();
        foreach ($questions as $key => $value) {
            if ($key != '_token') {
                if ($value) {
                    $skill = StudentsSkill::where('Opportunity__c', auth()->user()->opportunity_id)
                        ->where('Criteria__c', $key)
                        ->first();

                    if (!$skill) {
                        $skill = new StudentsSkill();
                        $skill->Criteria__c = $key;
                        $skill->Opportunity__c = auth()->user()->opportunity_id;
                    }
                    $skill->CriteriaValue__c = $value;
                    $skill->save();
                }
            }
        }
    }

    // TODO: Repo, Endpoint
    public function updateSalesforceData($request) {
        $account = Account::select('Id')->find($request->Id);
        $account->FirstName = $request->FirstName;
        $account->LastName = $request->LastName;
        $account->ShippingStreet = $request->ShippingStreet;
        $account->ShippingPostalCode = $request->ShippingPostalCode;
        $account->ShippingCity = $request->ShippingCity;
        $account->phone = $request->Phone;
        $account->save();
    }
}
