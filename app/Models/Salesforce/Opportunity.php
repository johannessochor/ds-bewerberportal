<?php

namespace App\Models\Salesforce;

use App\Traits\SalesforcePicklist;
use Lester\EloquentSalesForce\Model;

class Opportunity extends Model
{
    use SalesforcePicklist;

    private $salesForcePicklistMappings = [
        'Campus__c',
        'CourseOfStudy__c',
        'Applicant_Category__c'
    ];

    // TODO
    public function account()
    {
        return $this->belongsTo('App\Models\Salesforce\Account', 'AccountId', 'Id')
            ->select('Id', 'FirstName', 'LastName', 'ShippingStreet', 'ShippingCity',
                'ShippingCountry', 'ShippingPostalCode', 'Phone', 'EmailAddress1__c');
    }
}
