<?php

namespace App\Models\Salesforce;

use Lester\EloquentSalesForce\Model;

class MatchingCatalogOption extends Model
{
    public $table = 'MatchingCatalogOption__c';
}
