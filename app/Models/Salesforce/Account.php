<?php

namespace App\Models\Salesforce;

use App\Traits\SalesforcePicklist;
use Lester\EloquentSalesForce\Model;

class Account extends Model
{
    use SalesforcePicklist;

    private $salesForcePicklistMappings = [
        'Salutation',
        'CountryOfBirth__c',
        'Nationality__c'
    ];
}
