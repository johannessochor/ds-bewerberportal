<?php

namespace App\Models\Salesforce;

use Lester\EloquentSalesForce\Model;

class StudentsSkill extends Model
{
    protected $table = 'StudentsSkill__c';
}
