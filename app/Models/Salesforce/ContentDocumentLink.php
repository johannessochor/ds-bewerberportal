<?php

namespace App\Models\Salesforce;

use Lester\EloquentSalesForce\Model;

class ContentDocumentLink extends Model
{
    protected $table = 'ContentDocumentLink';
}

