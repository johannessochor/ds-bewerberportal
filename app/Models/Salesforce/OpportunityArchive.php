<?php

namespace App\Models\Salesforce;

use Lester\EloquentSalesForce\Model;

class OpportunityArchive extends Model
{
    protected $table = 'OpportunityArchive__c';
}
