<?php

namespace App\Models\Salesforce;

use Lester\EloquentSalesForce\Model;

class MatchingCatalogPicklistValue extends Model
{
    protected $table = 'MatchingCatalogPicklistValue__c';
}
