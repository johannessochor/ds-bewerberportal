<?php

namespace App\Http\Controllers;

use App\Services\SalesforceService;

class HomeController extends Controller
{
    /**
     * @var SalesforceService
     */
    private $salesforceService;

    /**
     * Create a new controller instance.
     *
     * @param SalesforceService $salesforceService
     */
    public function __construct(SalesforceService $salesforceService)
    {
        $this->middleware('auth');
        $this->salesforceService = $salesforceService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $applicant = $this->salesforceService->getSalesforceData();
        $advisor = $this->salesforceService->getOpportunityAdvisor($applicant['opportunity']['StudyAdvisor__c'] ?? null);

        return view('home', [
            'applicant' => $applicant,
            'user' => $advisor,
        ]);
    }
}
