<?php

namespace App\Http\Controllers;

use App\Services\SalesforceService;
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    /**
     * @var SalesforceService
     */
    private $salesforceService;

    /**
     * Create a new controller instance.
     *
     * @param SalesforceService $salesforceService
     */
    public function __construct(SalesforceService $salesforceService)
    {
        $this->middleware('auth');
        $this->salesforceService = $salesforceService;
    }

    // TODO
    public function index()
    {
        $questions = $this->salesforceService->getQuestionnaire();

        return view('questionnaire', [
            'questions' => $questions
        ]);
    }

    // TODO handle / validate Request
    public function update(Request $request)
    {
        $this->salesforceService->updateQuestionnaire($request);

        return redirect('/questionnaire?success=true');
    }
}
