<?php

namespace App\Http\Controllers;

use App\Models\Salesforce\Account;

class PracticalPartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // TODO create Service, Repo etc
    public function index()
    {
        // Example Coordinates cause Google Maps Integration is not active on UAT!
        $lat = 48.1353912;
        $long = 11.5278854;

        $accounts = Account::select('Id', 'Name', 'RecordTypeId', 'RecordType.Name',
            'ShippingLatitude', 'ShippingLongitude', 'ShippingPostalCode',
            'ShippingCity', 'ShippingStreet')
            ->where('RecordType.Name', 'B2B Partner')
            ->limit(10)
            ->get();

        foreach ($accounts as $account) {
            $account->distance = $this->calcDistance($lat, $long, $account->ShippingLatitude, $account->ShippingLongitude);
        }

        return view('practical-partner', [
            'accounts' => $accounts
        ]);
    }

    /**
     * TODO
     *
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @return float|int
     */
    private function calcDistance($lat1, $lon1, $lat2, $lon2)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;

            return ($miles * 1.609344);
        }
    }
}
