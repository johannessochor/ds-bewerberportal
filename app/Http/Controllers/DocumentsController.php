<?php

namespace App\Http\Controllers;

use App\Services\SalesforceService;
use Illuminate\Http\Request;

class DocumentsController extends Controller
{
    /**
     * @var SalesforceService
     */
    private $salesforceService;

    /**
     * Create a new controller instance.
     *
     * @param SalesforceService $salesforceService
     */
    public function __construct(SalesforceService $salesforceService)
    {
        $this->middleware('auth');
        $this->salesforceService = $salesforceService;
    }

    public function index()
    {
        return view('documents', [
            'files' => $this->salesforceService->getOpportunityFiles()
        ]);
    }

    // TODO
    public function file($id)
    {
        $file = $this->salesforceService->getFile($id);

        return response($file)->header('Content-Type', 'text/plain');
    }

    // TODO
    public function upload(Request $request)
    {
        $response = $this->salesforceService->fileUpload($request);

        if ($response['status']['success'] === true) {
            return redirect('/documents');
        }

        // For Debugging
        return $response;
    }
}
