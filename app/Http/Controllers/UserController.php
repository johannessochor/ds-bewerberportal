<?php

namespace App\Http\Controllers;

use App\Services\SalesforceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    /**
     * @var SalesforceService
     */
    private $salesforceService;

    /**
     * Create a new controller instance.
     *
     * @param SalesforceService $salesforceService
     */
    public function __construct(SalesforceService $salesforceService)
    {
        $this->middleware('auth');
        $this->salesforceService = $salesforceService;
    }

    public function index()
    {
        $applicant = $this->salesforceService->getSalesforceData();

        return view('user', [
            'applicant' => $applicant,
        ]);
    }

    public function updateAccount(Request $request)
    {
        $this->salesforceService->updateSalesforceData($request);
        Cache::flush(); // TODO not whole cache :)

        return redirect('/user?success=true');
    }
}
