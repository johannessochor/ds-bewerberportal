<?php

namespace App\Traits;

use Carbon\Carbon;
use DateTime;

trait SalesforcePicklist
{
    /**
     * Is used to map salesforce picklist values
     *
     * @return array
     */
    public function mapPicklistValues(): array
    {
        foreach ($this->attributes as $key => $value) {
            if (in_array($key, $this->salesForcePicklistMappings)) {
                $listValues = $this->getPicklistValues($key);
                if ($value) {
                    $this->attributes[$key] = $listValues[$value];
                }
            } elseif (!is_array($value) && $this->validateDate($value)) {
                $this->attributes[$key] = Carbon::parse($value)->format('d.m.Y');
            }
        }

        return $this->attributes;
    }

    /**
     * @param string $checkDate
     * @param string $format
     * @return bool
     */
    private function validateDate($checkDate, $format = 'Y-m-d'): bool
    {
        $date = DateTime::createFromFormat($format, $checkDate);

        return $date && $date->format($format) === $checkDate;
    }
}
