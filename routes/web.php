<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/questionnaire', 'QuestionnaireController@index');
Route::post('/questionnaire', 'QuestionnaireController@update');
Route::get('/practical-partner', 'PracticalPartnerController@index');
Route::get('/document/{id}', 'DocumentsController@file');
Route::get('/documents', 'DocumentsController@index');
Route::post('/documents', 'DocumentsController@upload');
Route::get('/user', 'UserController@index');
Route::post('/user', 'UserController@updateAccount');
